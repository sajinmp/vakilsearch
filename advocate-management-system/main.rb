require_relative 'options'

# Fetching modules and classes in lib and data_classes directories
Dir.glob('./data_classes/*.rb').each { |file| require file }
Dir.glob('./lib/*.rb').each { |file| require file }

include Utility

# Initializing global variables to store full data
$senior_lawyers, $junior_lawyers = [], []

# Loop unlimited times till exit is selected
loop do
  puts "\n\nOptions"
  puts OPTIONS
  option = get_input('Option', Integer)
  begin
    case option
    when 1
      senior = SeniorLawyer.new
      $senior_lawyers << senior
      puts senior.output
    when 2
      SeniorLawyer.add_junior
    when 3
      Lawyer.add_state
    when 4
      Lawyer.add_case
    when 5
      Lawyer.reject_case
    when 6
      $senior_lawyers.each { |lawyer| puts lawyer.output }
    when 7
      Utility.display_all_cases
    when 8
      Utility.display_all_cases(blocked: false)
    when 9
      Utility.display_cases_of_state
    else
      exit
    end
  rescue Exceptions::InvalidRecord
    puts "\n\nJunior lawyer already exists."
  rescue Exceptions::NotFound
    puts "\n\nLawyer could not be found."
  rescue Exceptions::BadRequest => e
    puts "\n\n#{e.message}"
  end
end
