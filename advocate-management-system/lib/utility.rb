require_relative 'exceptions'
Dir.glob('../data_classes/*.rb').each { |file| require file }

module Utility
  # Make all methods available to both class and instance objects
  def self.included(base)
    base.extend Methods
    base.include Methods
  end

  module Methods
    # Generic input method
    def get_input(attribute_name, return_type)
      print "Enter #{attribute_name}: "
      value = gets.chomp
      begin
        send(return_type.name, value)
      rescue ArgumentError
        return value.to_i if return_type == Integer
        puts '\nPlease check the value that you have entered'
      end
    end

    def check_if_lawyer_exists(id, type = nil)
      check_presence(set_lawyer_array(type), id)
    end

    def set_lawyer_array(type = nil)
      array = case type
              when 'senior'
                $senior_lawyers
              when 'junior'
                $junior_lawyers
              else
                ($senior_lawyers.dup << $junior_lawyers).flatten
              end
      array
    end

    def get_lawyer(lawyers, id)
      lawyers.find { |lawyer| lawyer.id == id }
    end

    def check_presence(lawyers, id)
      !get_lawyer(lawyers, id).nil?
    end

    def find_lawyer(id, type = nil)
      lawyer = get_lawyer(set_lawyer_array(type), id)
      raise Exceptions::NotFound unless lawyer
      lawyer
    end

    def get_all_cases(blocked)
      cases = Array.new
      if blocked
        set_lawyer_array.map { |lawyer| lawyer.senior? ? cases << lawyer.cases.dup << lawyer.blocked_cases : cases << lawyer.cases }
      else
        set_lawyer_array.map { |lawyer| cases << lawyer.cases }
      end
      Hash[cases.map(&:to_a).flatten.each_slice(2).to_a]
    end

    def display_all_cases(blocked: true)
      output_cases(get_all_cases(blocked))
    end

    def output_cases(cases)
      puts "ID - State"
      cases.each { |id, state| puts "#{id} - #{state}" }
    end

    def display_cases_of_state
      state = get_input('State', String)
      cases = get_all_cases(false)
      cases = Hash[cases.map(&:to_a).flatten.each_slice(2).to_a]
      cases.select! { |key, value| value == state }
      output_cases(cases)
    end
  end
end
