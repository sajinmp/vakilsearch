# This module contains the exceptions used
 
module Exceptions
  # If junior lawyer already exists
  class InvalidRecord < StandardError; end

  # When lawyer is not found
  class NotFound < StandardError; end

  # For other invalid operations
  class BadRequest < StandardError; end
end
