# Parent class for lawyer
require './lib/utility.rb'

class Lawyer
  include Utility
  attr_accessor :id, :name, :cases, :states

  def initialize(*args)
    @id = get_input('ID', Integer)
    @name = get_input('Name', String)
    @cases, @states = {}, []
  end

  # Class Methods
  def self.add_state
    lawyer = find_lawyer(get_input('Lawyer ID', Integer))
    if lawyer.senior?
      lawyer.states << get_input('State', String)
    else
      lawyer.add_state
    end
    puts lawyer.senior? ? lawyer.output : lawyer.senior.output
  end

  def self.add_case
    lawyer = find_lawyer(get_input('Lawyer ID', Integer))
    input_and_merge_case(lawyer)
    puts lawyer.senior? ? lawyer.output : lawyer.senior.output
  end

  def self.reject_case
    lawyer = find_lawyer(get_input('Senior Lawyer ID', Integer))
    raise Exceptions::BadRequest.new('Only senior can reject cases.') if lawyer.junior?
    input_and_merge_case(lawyer, type = 'blocked_cases')
    puts lawyer.output
  end

  def self.input_and_merge_case(lawyer, type = 'cases')
    case_id = get_input('Case ID', Integer)
    state = get_input('Practising State', String)
    lawyer.check_if_practicing_in_state(state)
    lawyer.check_if_blocked_case(case_id, state) if type == 'cases'
    lawyer.send(type).merge!({case_id => state})
  end

  # Instance Methods
  def senior?
    self.class.name == 'SeniorLawyer'
  end

  def junior?
    self.class.name == 'JuniorLawyer'
  end

  def check_if_practicing_in_state(state)
    raise Exceptions::BadRequest.new('Lawyer not practising in state.') unless @states.include?(state)
  end

  def check_if_blocked_case(case_id, state)
    if senior?
      raise Exceptions::BadRequest.new('Case already blocked by you.') if blocked?(case_id, state)
    else
      raise Exceptions::BadRequest.new('Case already blocked by Senior.') if senior.blocked?(case_id, state)
    end
  end

  ## Output methods
  def output
    <<~OUTPUT
    #{self.class.name}
      ID: #{@id}
      Name: #{@name}
      Practising States: #{output_states}
      Practising Cases: #{output_cases}
    OUTPUT
  end

  def output_states
    @states.join(', ')
  end

  def output_cases(cases = @cases)
    cases.map { |id, state| "#{id} - #{state}"}.join(', ')
  end

end
