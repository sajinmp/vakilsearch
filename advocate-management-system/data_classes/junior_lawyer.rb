require './data_classes/lawyer.rb'
require './data_classes/senior_lawyer.rb'

class JuniorLawyer < Lawyer
  attr_accessor :senior_id

  def initialize(*args)
    super
    @senior_id = args[0]
  end

  # Class Methods
  def self.output(ids)
    juniors = $junior_lawyers.select { |lawyer| ids.include?(lawyer.id) }
    juniors.map(&:output).join("\n")
  end

  # Instance methods
  def senior
    find_lawyer(@senior_id, 'senior')
  end

  def add_state
    state = get_input('State', String)
    raise Exceptions::BadRequest.new('Senior not practising in the state.') unless senior.states.include?(state)
    @states << state
  end
end
