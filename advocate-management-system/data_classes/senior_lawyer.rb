require_relative 'lawyer'
require_relative 'junior_lawyer'

class SeniorLawyer < Lawyer
  attr_accessor :juniors, :blocked_cases

  def initialize(*args)
    super
    @juniors, @blocked_cases = [], {}
  end

  # Class methods
  def self.add_junior
    senior = find_lawyer(get_input('Senior Laywer ID', Integer), 'senior')
    junior = JuniorLawyer.new(senior.id)
    raise Exceptions::InvalidRecord if check_if_lawyer_exists(junior.id, 'junior')
    senior.juniors << junior.id
    $junior_lawyers << junior
    puts senior.output
  end

  # Instance methods
  def blocked?(case_id, state)
    @blocked_cases.has_key?(case_id) && @blocked_cases.has_value?(state)
  end

  ## Output method
  def output
    puts <<~OUTPUT
      #{self.class.name}
      -----------------
      ID: #{@id}
      Name: #{@name}
      Practising States: #{output_states}
      Practising Cases: #{output_cases}
      Blocked Cases: #{output_cases(@blocked_cases)}
      #{JuniorLawyer.output(@juniors)}

    OUTPUT
  end
end
