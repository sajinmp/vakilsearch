# This file contains the options to be printed

OPTIONS = <<~OPTIONS
1. Add a senior lawyer
2. Add a junior lawyer
3. Add a state
4. Add a case
5. Reject a case
6. Display all lawyers
7. Display all cases
8. Display all unblocked cases
9. Display all cases under a state

OPTIONS
