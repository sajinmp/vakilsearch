def sudoku_checker(arr)
  arr.each_with_index do |row, i|
    # Check row
    return false unless valid_array(row)

    # Check Subsquares
    if [0, 3, 6].include?(i)
      [0, 3, 6].each do |start|
        return false unless valid_array(arr[i][start, 3] << arr[i + 1][start, 3] << arr[i + 2][start, 3])
      end
    end
  end

  # Check column
  arr.transpose.each do |column|
    return false unless valid_array(column)
  end
  true
end

def valid_array(arr)
  arr.flatten.sort == (1..9).to_a
end
