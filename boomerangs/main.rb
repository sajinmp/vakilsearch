def count_boomerangs(arr)
  arr.each_cons(3).inject(0) { |sum, i| sum += 1 if i[0] == i [-1] && i[1] != i[0]; sum }
end
