def join(arr)
  str, size = arr[0], arr.group_by(&:size).max.first
  non_overlapping_size = arr.join.size

  # Getting all consecutive sub arrays
  arr.each_cons(2) do |sub_arr|
    str_to_add, overlap_size = remove_overlap_and_return(*sub_arr)
    str += str_to_add unless str_to_add.nil? || str_to_add.empty?
    size = overlap_size if !overlap_size.nil? && overlap_size < size
  end

  return [str, 0] if str.size == non_overlapping_size
  [str, size]
end

def remove_overlap_and_return(first, second)
  size = first.size
  size.times { |i| return [second[(size - i)..-1], second[0..(size - i - 1)].size] if first[i..-1] == second[0..(size - i - 1)] }
  [second, nil]
end
