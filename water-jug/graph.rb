require 'byebug'

class Node
  attr_accessor :value, :children, :parent

  def initialize(value, parent = nil)
    @value = value
    @children = []
    @parent = parent
  end
  
  def add_child(child)
    @children << child
  end

  def pop_child
    @children.pop
  end
end

class Graph
  attr_accessor :root, :nodes, :current, :dist, :prev

  def initialize(value)
    @root = Node.new(value)
    @current = @root.dup
    @nodes = [@root]
    @dist = {}
    @prev = {}
  end
  
  def add_node(value)
    ret = true
    if node = find_node(value)
      current_dist = @dist[value].dup
      @current.add_child(node)
      calculate_distance
      if @dist[value].to_i >= current_dist.to_i
        ret = false
        @current.pop_child
      else
        @nodes << node
        @current = node
      end
    else
      node = Node.new(value, @current)
      @current.add_child(node)
      @nodes << node
      @current = node
    end
    calculate_distance
    ret
  end

  def set_current_to_parent
    @current = @current.parent
  end

  def find_node(value)
    @nodes.find { |i| i.value == value }
  end

  def index(node)
    @nodes.index(node)
  end

  def calculate_distance
    @dist = {}
    @prev = {}
    @nodes.each do |node|
      @dist[node.value] = Float::INFINITY
      @prev[node.value] = -1
    end
    @dist[@root.value] = 0
    unvisited = @nodes.dup

    while unvisited.size > 0
      nod = nil
      unvisited.each do |node|
        nod = node if nod.nil? || (@dist[node.value] && @dist[node.value] < @dist[nod.value])
      end

      break if @dist[nod.value] == Float::INFINITY
      unvisited -= [nod]

      nod.children.each do |v|
        alt = @dist[nod.value] + 1

        if alt < @dist[v.value]
          @dist[v.value] = alt
          @prev[v.value] = nod
        end
      end
    end
  end

  def get_steps_to(target)
    calculate_distance
    @dist.each { |k, v| p "#{k} - #{v}" }
    @dist[target]
  end

  def print
    str = Array.new
    print_children(str, @root)
    p str
  end

  def print_children(str, node)
    str << node.value
    node.children.each do |child|
      print_children(str, child)
    end
  end
end
