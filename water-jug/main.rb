# def waterjug(capacity, target)
$x, $y, $z = [3, 5, 8]
initial_state = [0, 0, 8]
$target = [0, 3, 5]
$visited = []
$steps = []
# end

def add_step(state)
  $steps << state
  return true
end

def get_states(*state)
  a, b, c = state
  if [a, b, c] == $target
    return add_step(state)
  end

  if $visited.include?(state)
    return false
  end

  $visited << state

  if c > 0
    if b + c <= $y
      return add_step(state) if get_states(a, b + c, 0)
    else
      return add_step(state) if get_states(a, $y, c - $y - b)
    end

    if a + c <= $x
      return add_step(state) if get_states(a + c, b, 0)
    else
      return add_step(state) if get_states($x, b, c - $z - a)
    end
  end

  if b > 0
    if b + c <= $z
      return add_step(state) if get_states(a, 0, b + c)
    else
      return add_step(state) if get_states(a, b - $z - c, $z)
    end

    if a + b <= $x
      return add_step(state) if get_states(a + b, 0, c)
    else
      return add_step(state) if get_states($x, b - $z - a, c)
    end
  end

  if a > 0
    if a + c <= $z
      return add_step(state) if get_states(0, b, a + c)
    else
      return add_step(state) if get_states(a - $z - c, b, $z)
    end

    if a + b <= $y
      return add_step(state) if get_states(0, a + b, c)
    else
      return add_step(state) if get_states(a - $y - b, $y, c)
    end
  end

  false
end

get_states(*initial_state)
p $visited
p $steps.reverse
