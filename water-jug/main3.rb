require 'rgl/adjacency'
require 'rgl/path'
require 'rgl/dijkstra'
require 'byebug'

module WaterJug
  def waterjug(capacity, target)
    $capacity = capacity
    $dist = {}
    initial_state = [0, 0, capacity[-1]]
    $graph = RGL::DirectedAdjacencyGraph.new
    $graph.add_vertex(initial_state)
    pour_water(initial_state)
    path = $graph.dijkstra_shortest_path($dist, initial_state, target)
    if path
      return path.length - 1
    else
      return "No Solution"
    end
  end

  def pour_water(state)
    pourable_jugs = state.map.with_index { |i, index| [i, index] if i > 0 }
    pourable_jugs.compact.each_with_index do |jug, index|
      other_jugs = state.map.with_index { |v, i| [v, i] unless i == jug[1] }
      other_jugs.compact.each do |val|
        pour(jug[1], val[1], state)
      end
    end
  end

  def pour(from, to, state)
    next_state = state.dup
    if state[from] + state[to] <= $capacity[to]
      next_state[to] += state[from]
      next_state[from] = 0
    else
      next_state[from] -= $capacity[to] - state[to]
      next_state[to] = $capacity[to]
    end
    if add_vertex_and_edge(state, next_state)
      pour_water(next_state)
    end
  end

  def add_vertex_and_edge(from, to)
    $graph.add_vertex(to)
    $dist[[from, to]] = 1
    if $graph.has_vertex?(to) && $graph.path?(from , to)
      $graph.add_edge(from, to)
      return false
    else
      $graph.add_edge(from, to)
      return true
    end
  end
end
