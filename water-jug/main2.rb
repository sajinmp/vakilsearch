require_relative 'graph'

module WaterJug
  def waterjug(capacity, target)
    $capacity = capacity
    initial_state = [0, 0, capacity[-1]]
    target = target
    $graph = Graph.new(initial_state)
    generate_graph(*initial_state)
    puts $graph.get_steps_to(target)
  end
  
  def generate_graph(*state)
    $graph.add_node(state)
    pour_water(*state)
  end
  
  def pour_water(*state)
    val = false
    pourable_jugs = state.map.with_index { |i, index| [i, index] if i > 0 }
    pourable_jugs.compact.each_with_index do |jug, index|
      other_jugs = state.map.with_index { |v, i| [v, i] unless i == jug[1] }
      other_jugs.compact.each do |val|
        val = pour(jug[1], val[1], state)
        break unless val
      end
    end
    $graph.set_current_to_parent unless val
  end
  
  def pour(from, to, state)
    dupli = state.dup
    if pourable(from, to, dupli)
      dupli[to] += dupli[from]
      dupli[from] = 0
    else
      dupli[from] -= $capacity[to] - state[to]
      dupli[to] = $capacity[to]
    end
    if $graph.add_node(dupli)
      pour_water(*dupli)
    end
    if $graph.current.value == $graph.root.value
      return false
    else
      return true
    end
  end
  
  def pourable(from, to, state)
    state[from] + state[to] <= $capacity[to]
  end
end
